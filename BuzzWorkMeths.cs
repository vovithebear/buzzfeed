﻿using System;

namespace BuzzFeed
{
    class BuzzWorkMeths
    {
        string buzz = "buzz";
        string feed = "feed";
        string buzzfeed = "buzzfeed";

        public void buzzfeedworks()
        {
            for (int number = 1; number <= 100; number++)
            {
                if (number % 3 == 0 && number % 5 == 0)
                {
                    Console.WriteLine($"{number} {buzzfeed}");
                }
                else if (number % 3 == 0)
                {
                    Console.WriteLine($"{number} {buzz}");
                }
                else if (number % 5 == 0)
                {
                    Console.WriteLine($"{number} {feed}");
                }
                else
                {
                    Console.WriteLine($"{number} Not divisible by either 3 or 5 or both");
                }
            }
        }
    }
}
